# snowflakeclient

This is a pre-built AAR package of the Go Pluggable Transports client from the Snowflake project.

Android build project:
https://gitlab.torproject.org/eighthave/snowflake

Snowflake project:
https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake
